Requrements
===========
PHP >= 5.6 - I use short array declaration

Installation
============

Install dependencies
```bash
cd /path/to/project
# Install composer dependencies
composer install
npm install
bower install
```

Database stored in dump symfony-test.sql at the root of project

Tests
=====
I provided some demo tests. U can launch them by
```bash
phpunit tests/AppBundle/Service
```

Frontend
========
Because of this is demo, I use a little SASS only for demonstration.
JS is written with CoffeeScript compiled via browserify for module support and Gulp task runner.
Previous version of JS on ES6/Babel/Marionette replaced to 'web/js/src-es6-not used' dir.
If u need make some changes in frontend, u can do it simple by
```bash
gulp
```
This will add a watcher and run tasks automatically