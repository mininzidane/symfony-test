var gulp = require('gulp');
var browserify = require('browserify');
// var babelify = require("babelify");
var fs = require("fs");
var compass = require('gulp-compass');
var coffeeify = require('coffeeify');
var uglify = require('gulp-uglify');

var watch = function () {
	gulp.watch('web/**/*', ['build']);
};

var build = function () {
	console.log('Run SASS');
	gulp
		.src('web/css/sass/main.scss')
		.pipe(compass({
			project: __dirname,
			//config_file: getPathFromPublic('config.rb'), // it could be unnecessary
			image: 'images',
			css: 'web/css/',
			style: 'compressed',
			sourcemap: true, // todo add concatted source map support
			sass: 'web/css/sass'
		}));

	console.log('Run browserify');
	browserify({debug: true})
		// .transform(babelify, {presets: ['es2015']})
		.transform(coffeeify)
		.transform('jstify')
		.require('web/js/src/app.coffee', {entry: true})
		.bundle()
		.on("error", function (err) {
			console.log("Error: " + err.message);
		})
		.pipe(fs.createWriteStream('web/js/app.js'));
};

var start = function () {
	build();
	watch();
};

gulp.task('build', function () {
	return build();
});
gulp.task('default', function () {
	return start();
});
// for prod
gulp.task('uglify', function () {
	gulp.src('web/js/app.js')
		.pipe(uglify())
		.pipe(gulp.dest('web/js'));
});