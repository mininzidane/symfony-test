<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Album;
use AppBundle\Service\AlbumProvider;
use AppBundle\Service\ImageProvider;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class AjaxController extends Controller {

	const STATUS_OK    = 'ok';
	const STATUS_ERROR = 'error';

	/**
	 * Wrapper for serialize some object to JSON
	 *
	 * @param string $data
	 * @param string $status
	 * @return Response
	 */
	private function getJSONResponse($data, $status = self::STATUS_OK) {
		$encoder    = new JsonEncoder();
		$normalizer = new ObjectNormalizer();

		$normalizer->setCircularReferenceHandler(function ($object) {
			return $object->getId();
		});
		/** @var Serializer $serializer */
		$serializer = new Serializer([$normalizer], [$encoder]);
		$json       = $serializer->serialize([
			'status' => $status,
			'data'   => $data,
		], 'json');

		return new Response($json, 200, [
			'Content-Type' => 'application/json; charset=utf-8',
		]);
	}

	/**
	 * Get album list
	 * @Route("/ajax/album-list", name="ajax.album-list")
	 */
	public function albumListAction() {
		/** @var AlbumProvider $albumProvider */
		$albumProvider = $this->get('AlbumProvider');
		$albums = $albumProvider->getAlbums();

		if (count($albums) == 0) {
			return $this->getJSONResponse("Albums not found", self::STATUS_ERROR);
		}

		return $this->getJSONResponse($this->renderView('ajax/album-list.twig', [
			'albums' => $albums,
		]));
	}

	/**
	 * Get images from album
	 * @Route("/ajax/album/{id}", name="ajax.album-item", requirements={"id": "\d+"})
	 * @Route("/ajax/album/{id}/page/{page}", name="ajax.album-item-pagination", requirements={"id": "\d+", "page": "\d*"})
	 * @Route("/album/{id}/page/{page}", name="ajax.hash.album-item-pagination", requirements={"id": "\d+", "page": "\d*"})
	 * @param int $id
	 * @param int $page
	 * @return Response
	 */
	public function albumItemAction($id, $page = 1) {
		if (!$page) {
			$page = 1;
		}
		/** @var AlbumProvider $albumProvider */
		$albumProvider = $this->get('AlbumProvider');
		$images = $albumProvider->getImagesByAlbum($id, $page);

		if ($images === null) {
			return $this->getJSONResponse("Images not found by album id: {$id}", self::STATUS_ERROR);
		}

		return $this->getJSONResponse($this->renderView('ajax/album-detail.twig', [
			'images' => $images,
		]));
	}
}
