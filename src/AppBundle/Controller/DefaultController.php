<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Album;
use AppBundle\Service\ImageProvider;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller {

	/**
	 * @Route("/", name="homepage")
	 */
	public function indexAction() {
		return $this->render('default/index.twig');
	}
}