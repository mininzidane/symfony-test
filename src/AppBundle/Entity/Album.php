<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Knp\Component\Pager\Paginator;

/**
 * Album
 *
 * @ORM\Table(name="album")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AlbumRepository")
 */
class Album {

	const IMAGES_PER_PAGE = 10;

	/**
	 * @var int
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="title", type="string", length=128)
	 */
	private $title;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="created", type="datetime")
	 */
	private $created;

	/**
	 * @var int
	 */
	private $currentImagesPage = 1;

	/**
	 * @var int
	 */
	private $imagesPerPage = self::IMAGES_PER_PAGE;

	/**
	 * @var int
	 */
	private $imagesCount;

	/**
	 * @ORM\OneToMany(targetEntity="Image", mappedBy="album")
	 */
	private $images;

	public function __construct() {
		$this->images = new ArrayCollection();
	}

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set title
	 *
	 * @param string $title
	 * @return Album
	 */
	public function setTitle($title) {
		$this->title = $title;

		return $this;
	}

	/**
	 * Get title
	 *
	 * @return string
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * Get created
	 *
	 * @return \DateTime
	 */
	public function getCreated() {
		return $this->created;
	}

	/**
	 * @return Image[]
	 */
	public function getImages() {
		return $this
			->images
			->slice(($this->currentImagesPage - 1) * self::IMAGES_PER_PAGE, self::IMAGES_PER_PAGE);
	}

	/**
	 * Get related images count
	 * @return int
	 */
	public function getImagesCount() {
		if ($this->imagesCount === null) {
			$this->imagesCount = $this->images->count();
		}
		return $this->imagesCount;
	}

	public function getImagesPerPage() {
		return $this->imagesPerPage;
	}

	/**
	 * Set page for images pagination
	 * @param int $page
	 * @return $this
	 */
	public function setCurrentImagePage($page) {
		$this->currentImagesPage = $page;
		return $this;
	}

	public function getCurrentImagesPage() {
		return (int) $this->currentImagesPage;
	}
}