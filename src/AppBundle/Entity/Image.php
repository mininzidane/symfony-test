<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Image
 *
 * @ORM\Table(name="image")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ImageRepository")
 */
class Image {

	/**
	 * @var int
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="title", type="string", length=255)
	 */
	private $title;

	/**
	 * @var int
	 *
	 * @ORM\Column(name="albumId", type="integer")
	 */
	private $albumId;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="filename", type="string", length=255)
	 */
	private $filename;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="created", type="datetime")
	 */
	private $created;

	/**
	 * @ORM\ManyToOne(targetEntity="Album", inversedBy="images")
	 * @ORM\JoinColumn(name="albumId", referencedColumnName="id")
	 */
	private $album;

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set title
	 *
	 * @param string $title
	 * @return Image
	 */
	public function setTitle($title) {
		$this->title = $title;

		return $this;
	}

	/**
	 * Get title
	 *
	 * @return string
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * Set albumId
	 *
	 * @param integer $albumId
	 * @return Image
	 */
	public function setAlbumId($albumId) {
		$this->albumId = $albumId;

		return $this;
	}

	/**
	 * Get albumId
	 *
	 * @return integer
	 */
	public function getAlbumId() {
		return $this->albumId;
	}

	/**
	 * Set filename
	 *
	 * @param string $filename
	 * @return Image
	 */
	public function setFilename($filename) {
		$this->filename = $filename;

		return $this;
	}

	/**
	 * Get filename
	 *
	 * @return string
	 */
	public function getFilename() {
		return $this->filename;
	}

	/**
	 * Get created
	 *
	 * @return \DateTime
	 */
	public function getCreated() {
		return $this->created;
	}

	/**
	 * @return Album
	 */
	public function getAlbum() {
		return $this->album;
	}
}
