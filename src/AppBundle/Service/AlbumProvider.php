<?php

namespace AppBundle\Service;

use Doctrine\ORM\EntityManager;
use Knp\Component\Pager\Paginator;

class AlbumProvider {

	const LIMIT = 10;

	private $_em;
	private $_pagination;

	public function __construct(EntityManager $em, Paginator $pagination) {
		$this->_em         = $em;
		$this->_pagination = $pagination;
	}

	/**
	 * Get all albums data
	 *
	 * @return array
	 */
	public function getAlbums() {
		$connection = $this->_em->getConnection();
		$sql = "
		SELECT * FROM `album` `a`
		INNER JOIN (
		SELECT `albumId`, COUNT(*) AS `imageCount` FROM `image` GROUP BY `albumId`
		) AS `imageCount`
		ON `a`.`id` = `imageCount`.`albumId`
		";
		$statement  = $connection->prepare($sql);
		$statement->execute();
		return $statement->fetchAll();
	}

	/**
	 * Get paginated images info for specified album
	 *
	 * @param int $albumId
	 * @param int $page
	 * @param int $limit
	 * @return \Knp\Component\Pager\Pagination\PaginationInterface|null
	 */
	public function getImagesByAlbum($albumId, $page = 1, $limit = self::LIMIT) {
		$connection = $this->_em->getConnection();
		$sql = "
		SELECT `image`.*, `album`.`title` AS `albumTitle` FROM `image`
		LEFT JOIN `album` ON `album`.`id` = `image`.`albumId`
		WHERE `albumId` = :id
		";
		$statement  = $connection->prepare($sql);
		$statement->bindValue('id', $albumId);
		$statement->execute();
		$results = $statement->fetchAll();
		if (count($results) == 0) {
			return null;
		}
//		$query = $this->em
//			->createQuery("SELECT i, a.title AS albumTitle FROM AppBundle:Image i JOIN i.album a WHERE i.albumId = :id")
//			->setParameter(':id', $albumId);

//		$pagination = $this->pagination->paginate($query, $page, $limit);
		$pagination = $this->_pagination->paginate($results, $page, $limit);
		$pagination->setUsedRoute('ajax.hash.album-item-pagination');
		return $pagination;
	}
}