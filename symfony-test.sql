-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               10.1.16-MariaDB - mariadb.org binary distribution
-- ОС Сервера:                   Win32
-- HeidiSQL Версия:              9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры для таблица symfony-test.album
CREATE TABLE IF NOT EXISTS `album` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы symfony-test.album: ~5 rows (приблизительно)
/*!40000 ALTER TABLE `album` DISABLE KEYS */;
INSERT IGNORE INTO `album` (`id`, `title`, `created`) VALUES
	(1, 'Sea', '2016-10-05 12:27:59'),
	(2, 'Sky', '2016-10-05 12:27:59'),
	(3, 'Snow', '2016-10-05 12:27:59'),
	(4, 'Rain', '2016-10-05 12:27:59'),
	(5, 'Grass', '2016-10-05 12:27:59');
/*!40000 ALTER TABLE `album` ENABLE KEYS */;


-- Дамп структуры для таблица symfony-test.image
CREATE TABLE IF NOT EXISTS `image` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `albumId` int(11) unsigned NOT NULL,
  `filename` varchar(255) NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_Image_album` (`albumId`),
  CONSTRAINT `FK_Image_album` FOREIGN KEY (`albumId`) REFERENCES `album` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы symfony-test.image: ~5 rows (приблизительно)
/*!40000 ALTER TABLE `image` DISABLE KEYS */;
INSERT IGNORE INTO `image` (`id`, `title`, `albumId`, `filename`, `created`) VALUES
	(2, 'Sky 1', 2, '/images/sky-1.jpeg', '2016-10-05 13:19:32'),
	(3, 'Grass 1', 5, '/images/grass-1.jpeg', '2016-10-05 13:19:32'),
	(4, 'Rain 1', 4, '/images/rain-1.jpeg', '2016-10-05 13:19:32'),
	(5, 'Sea 1', 1, '/images/sea-1.jpg', '2016-10-05 13:19:32'),
	(6, 'Snow 1', 3, '/images/snow-1.jpeg', '2016-10-05 13:19:32'),
	(7, 'Sky 2', 2, '/images/sky-2.jpg', '2016-10-05 13:19:32'),
	(8, 'Sky 3', 2, '/images/sky-3.jpeg', '2016-10-05 13:19:32'),
	(9, 'Sea 2', 1, '/images/sea-2.jpg', '2016-10-05 13:19:32'),
	(10, 'Sea 3', 1, '/images/sea-3.jpg', '2016-10-05 13:19:32'),
	(11, 'Snow 2', 3, '/images/snow-2.jpeg', '2016-10-05 13:19:32'),
	(12, 'Snow 3', 3, '/images/snow-3.jpeg', '2016-10-05 13:19:32'),
	(13, 'Rain 2', 4, '/images/rain-2.jpeg', '2016-10-05 13:19:32'),
	(14, 'Rain 3', 4, '/images/rain-3.jpg', '2016-10-05 13:19:32'),
	(15, 'Grass 2', 5, '/images/grass-1.jpeg', '2016-10-05 13:19:32'),
	(16, 'Grass 3', 5, '/images/grass-3.jpeg', '2016-10-05 13:19:32');

INSERT INTO `image` (`title`, `albumId`, `filename`) VALUES
  -- Sea [1]
  ('Sea 1', 1, '/images/sea-1.jpg'),
  ('Sea 3', 1, '/images/sea-3.jpg'),


  -- Sky [2]
	('Sky 1', 2, '/images/sky-1.jpeg'),
  ('Sky 2', 2, '/images/sky-2.jpg'),
  ('Sky 3', 2, '/images/sky-3.jpeg'),
  ('Sky 1', 2, '/images/sky-1.jpeg'),
  ('Sky 2', 2, '/images/sky-2.jpg'),
  ('Sky 3', 2, '/images/sky-3.jpeg'),
  ('Sky 2', 2, '/images/sky-2.jpg'),
  ('Sky 2', 2, '/images/sky-2.jpg'),
  ('Sky 2', 2, '/images/sky-2.jpg'),
  ('Sky 1', 2, '/images/sky-1.jpeg'),
  ('Sky 2', 2, '/images/sky-2.jpg'),
  ('Sky 1', 2, '/images/sky-1.jpeg'),
  ('Sky 2', 2, '/images/sky-2.jpg'),
  ('Sky 1', 2, '/images/sky-1.jpeg'),
  ('Sky 2', 2, '/images/sky-2.jpg'),
  ('Sky 1', 2, '/images/sky-1.jpeg'),
  ('Sky 3', 2, '/images/sky-3.jpeg'),
  ('Sky 2', 2, '/images/sky-2.jpg'),
  ('Sky 2', 2, '/images/sky-2.jpg'),
  ('Sky 1', 2, '/images/sky-1.jpeg'),
  ('Sky 2', 2, '/images/sky-2.jpg'),
  ('Sky 2', 2, '/images/sky-2.jpg'),
  ('Sky 2', 2, '/images/sky-2.jpg'),
  ('Sky 1', 2, '/images/sky-1.jpeg'),
  ('Sky 1', 2, '/images/sky-1.jpeg'),
  ('Sky 1', 2, '/images/sky-1.jpeg'),
  ('Sky 3', 2, '/images/sky-3.jpeg'),
  ('Sky 1', 2, '/images/sky-1.jpeg'),

  -- Snow [3]
  ('Snow 1', 3, '/images/snow-1.jpeg'),
  ('Snow 2', 3, '/images/snow-2.jpeg'),
  ('Snow 3', 3, '/images/snow-3.jpeg'),
  ('Snow 1', 3, '/images/snow-1.jpeg'),
  ('Snow 2', 3, '/images/snow-2.jpeg'),
  ('Snow 2', 3, '/images/snow-2.jpeg'),
  ('Snow 3', 3, '/images/snow-3.jpeg'),
  ('Snow 3', 3, '/images/snow-3.jpeg'),
  ('Snow 2', 3, '/images/snow-2.jpeg'),
  ('Snow 1', 3, '/images/snow-1.jpeg'),
  ('Snow 2', 3, '/images/snow-2.jpeg'),
  ('Snow 3', 3, '/images/snow-3.jpeg'),
  ('Snow 3', 3, '/images/snow-3.jpeg'),
  ('Snow 3', 3, '/images/snow-3.jpeg'),
  ('Snow 3', 3, '/images/snow-3.jpeg'),
  ('Snow 1', 3, '/images/snow-1.jpeg'),
  ('Snow 2', 3, '/images/snow-2.jpeg'),
  ('Snow 1', 3, '/images/snow-1.jpeg'),
  ('Snow 3', 3, '/images/snow-3.jpeg'),
  ('Snow 3', 3, '/images/snow-3.jpeg'),
  ('Snow 3', 3, '/images/snow-3.jpeg'),
  ('Snow 3', 3, '/images/snow-3.jpeg'),
  ('Snow 2', 3, '/images/snow-2.jpeg'),
  ('Snow 2', 3, '/images/snow-2.jpeg'),
  ('Snow 2', 3, '/images/snow-2.jpeg'),
  ('Snow 2', 3, '/images/snow-2.jpeg'),
  ('Snow 3', 3, '/images/snow-3.jpeg'),
  ('Snow 3', 3, '/images/snow-3.jpeg'),
  ('Snow 3', 3, '/images/snow-3.jpeg'),
  ('Snow 1', 3, '/images/snow-1.jpeg'),
  ('Snow 1', 3, '/images/snow-1.jpeg'),


  -- Rain [4]
  ('Rain 1', 4, '/images/rain-1.jpeg'),
  ('Rain 2', 4, '/images/rain-2.jpeg'),
  ('Rain 3', 4, '/images/rain-3.jpg'),
  ('Rain 1', 4, '/images/rain-1.jpeg'),
  ('Rain 2', 4, '/images/rain-2.jpeg'),
  ('Rain 2', 4, '/images/rain-2.jpeg'),
  ('Rain 2', 4, '/images/rain-2.jpeg'),
  ('Rain 2', 4, '/images/rain-2.jpeg'),
  ('Rain 3', 4, '/images/rain-3.jpg'),
  ('Rain 3', 4, '/images/rain-3.jpg'),
  ('Rain 1', 4, '/images/rain-1.jpeg'),
  ('Rain 1', 4, '/images/rain-1.jpeg'),
  ('Rain 1', 4, '/images/rain-1.jpeg'),
  ('Rain 2', 4, '/images/rain-2.jpeg'),
  ('Rain 3', 4, '/images/rain-3.jpg'),
  ('Rain 3', 4, '/images/rain-3.jpg'),
  ('Rain 3', 4, '/images/rain-3.jpg'),
  ('Rain 2', 4, '/images/rain-2.jpeg'),
  ('Rain 2', 4, '/images/rain-2.jpeg'),
  ('Rain 1', 4, '/images/rain-1.jpeg'),
  ('Rain 3', 4, '/images/rain-3.jpg'),
  ('Rain 3', 4, '/images/rain-3.jpg'),
  ('Rain 1', 4, '/images/rain-1.jpeg'),
  ('Rain 1', 4, '/images/rain-1.jpeg'),
  ('Rain 2', 4, '/images/rain-2.jpeg'),
  ('Rain 2', 4, '/images/rain-2.jpeg'),
  ('Rain 3', 4, '/images/rain-3.jpg'),
  ('Rain 3', 4, '/images/rain-3.jpg'),
  ('Rain 3', 4, '/images/rain-3.jpg'),
  ('Rain 3', 4, '/images/rain-3.jpg'),
  ('Rain 2', 4, '/images/rain-2.jpeg'),
  ('Rain 2', 4, '/images/rain-2.jpeg'),
  ('Rain 1', 4, '/images/rain-1.jpeg'),
  ('Rain 2', 4, '/images/rain-2.jpeg'),
  ('Rain 2', 4, '/images/rain-2.jpeg'),


  -- Grass [5]
  ('Grass 1', 5, '/images/grass-1.jpeg'),
  ('Grass 2', 5, '/images/grass-1.jpeg'),
  ('Grass 2', 5, '/images/grass-1.jpeg'),
  ('Grass 1', 5, '/images/grass-1.jpeg'),
  ('Grass 2', 5, '/images/grass-1.jpeg'),
  ('Grass 1', 5, '/images/grass-1.jpeg'),
  ('Grass 2', 5, '/images/grass-1.jpeg'),
  ('Grass 1', 5, '/images/grass-1.jpeg'),
  ('Grass 3', 5, '/images/grass-3.jpeg'),
  ('Grass 1', 5, '/images/grass-1.jpeg'),
  ('Grass 2', 5, '/images/grass-1.jpeg'),
  ('Grass 1', 5, '/images/grass-1.jpeg'),
  ('Grass 3', 5, '/images/grass-3.jpeg'),
  ('Grass 2', 5, '/images/grass-1.jpeg'),
  ('Grass 2', 5, '/images/grass-1.jpeg'),
  ('Grass 2', 5, '/images/grass-1.jpeg'),
  ('Grass 3', 5, '/images/grass-3.jpeg'),
  ('Grass 2', 5, '/images/grass-1.jpeg'),
  ('Grass 1', 5, '/images/grass-1.jpeg'),
  ('Grass 3', 5, '/images/grass-3.jpeg'),
  ('Grass 2', 5, '/images/grass-1.jpeg'),
  ('Grass 3', 5, '/images/grass-3.jpeg'),
  ('Grass 2', 5, '/images/grass-1.jpeg'),
  ('Grass 3', 5, '/images/grass-3.jpeg'),
  ('Grass 2', 5, '/images/grass-1.jpeg'),
  ('Grass 3', 5, '/images/grass-3.jpeg');

/*!40000 ALTER TABLE `image` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
