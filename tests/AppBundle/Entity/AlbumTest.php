<?php

namespace Tests\AppBundle\Entity;

use AppBundle\Entity\Album;

class AlbumTest extends \PHPUnit_Framework_TestCase {

	public function testPagination() {
		/** @var Album $mock */
		$mock = $this->getMockBuilder('AppBundle\Entity\Album')
			->setMethods()
			->getMock();

		$mock->setCurrentImagePage(5);
		$this->assertEquals(5, $mock->getCurrentImagesPage());
	}

	public function testImages() {
		/** @var Album $mock */
		$mock = $this->getMockBuilder('AppBundle\Entity\Album')
			->setMethods()
			->getMock();
		$this->assertInternalType('array', $mock->getImages());
	}
}