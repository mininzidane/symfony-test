<?php

namespace Tests\AppBundle\Service;

use AppBundle\Service\AlbumProvider;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AlbumProviderTest extends WebTestCase {

	/** @var AlbumProvider */
	private $_provider;

	protected function setUp() {
		parent::setUp();
		self::bootKernel();

		$this->_provider = static::$kernel->getContainer()
			->get('AlbumProvider');
	}

	public function testGetAlbums() {
		$albums = $this->_provider->getAlbums();
		$this->assertInternalType('array', $albums);
	}

	public function testGetImagesByAlbum() {
		$images = $this->_provider->getImagesByAlbum(99999);
		$this->assertNull($images);

		$images = $this->_provider->getImagesByAlbum(1);
		$this->assertInstanceOf('\Knp\Component\Pager\Pagination\PaginationInterface', $images);
	}
}