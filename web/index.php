<?php
$prodMode = 'prod';
defined('APPLICATION_MODE') or define('APPLICATION_MODE', isset($_SERVER['APPLICATION_MODE'])? $_SERVER['APPLICATION_MODE']: $prodMode);

require(APPLICATION_MODE == $prodMode? 'app.php': 'app_dev.php');