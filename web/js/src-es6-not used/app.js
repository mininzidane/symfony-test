import Backbone from 'backbone';
import Marionette from 'backbone.marionette';
import AlbumListView from './views/AlbumList'
import Albums from './models/Albums'
import AlbumDetailView from './views/AlbumDetail'
import Album from './models/Album'

var AlbumController = {
	list() {
		var albums = new Albums();
		albums.fetch({
			reset: true,
			success() {
				app.getRegion().show(new AlbumListView(albums))
			}
		});
	},
	detail(albumId, page = 1) {
		var album = new Album({id: albumId, page: page});
		album.fetch({
			reset: true,
			success() {
				app.getRegion().show(new AlbumDetailView({model: album}))
			}
		})
	}
};

var Router = Marionette.AppRouter.extend({
	controller: AlbumController,

	appRoutes: {
		'': 'list',
		'album/:albumId': 'detail',
		'album/:albumId/page/:page': 'detail',
	}
});

var App = Marionette.Application.extend({
	region: '#app',

	onStart: function () {

	}
});

var app = new App({
	routes: new Router()
});

// Start history when our application is ready
app.on('start', function () {
	Backbone.history.start();
});

// loadInitialData().then(app.start);
app.start();

export { app };