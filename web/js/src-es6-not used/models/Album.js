import Backbone from 'backbone';

var Album = Backbone.Model.extend({
	url: function () {
		return '/ajax/album/' + this.id + '/page/' + this.page;
	},

	initialize(params) {
		this.page = params.page || 1;
	}
});

export default Album