import Backbone from 'backbone';

var Albums = Backbone.Collection.extend({
	url: '/ajax/album-list'
});

export default Albums