import Marionette from 'backbone.marionette';
import _ from 'underscore';
import getPagination from './pagination'

var tpl =
	'<div class="page-header"><h1><%= album.get(\'title\') %></h1></div>' +
	'<div class="row">' +
		'<% i = 1; %>' +
		'<% _.each(album.get(\'images\'), function(item) { %>' +
		'<div class="col-sm-3">' +
			'<h3><%= item.title %></h3>' +
			'<img class="mw-100" src="<%= item.filename %>" alt="<%= item.title %>">' +
		'</div>' +
		'<% if (i % 4 == 0) { %>' +
	'</div>' +
	'<div class="row">' +
		'<% } %>' +
		'<% i++; %>' +
		'<% }) %>' +
	'</div>';

var AlbumDetailView = Marionette.View.extend({
	initialize(params) {
		this.album = params.model;
		this.render()
	},

	render() {
		this.$el.html(_.template(tpl)({
			album: this.album
		}) + getPagination(
				'#album/' + this.album.id + '/page/',
				this.album.get('imagesCount'),
				this.album.get('imagesPerPage'),
				this.album.get('currentImagesPage')
			)
		);
	}
});

export default AlbumDetailView;