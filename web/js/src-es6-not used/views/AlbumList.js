import Marionette from 'backbone.marionette';
import _ from 'underscore';

var tpl =
	'<div class="page-header"><h1>Gallery list</h1></div>' +
	'<table class="table">' +
		'<tr>' +
			'<th>ID</th>' +
			'<th>Name</th>' +
			'<th>Image count</th>' +
		'</tr>' +
		'<% _.each(items, function(item) { %>' +
		'<tr>' +
			'<td><%= item.get(\'id\') %></td>' +
			'<td><a href="#album/<%= item.get(\'id\') %>"><%= item.get(\'title\') %></a></td>' +
			'<td><%= item.get(\'imagesCount\') %> images</td>' +
		'</tr>' +
		'<% }) %>' +
	'</table>';

var AlbumListView = Marionette.View.extend({
	initialize(albums) {
		this.albums = albums.models;
		this.render()
	},

	render() {
		this.$el.html(_.template(tpl)({
			items: this.albums
		}))
	}
});

export default AlbumListView;