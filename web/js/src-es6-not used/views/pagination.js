import _ from 'underscore';

/**
 * Build bootstrap pagination
 * @param {String} url
 * @param {Number} itemCount
 * @param {Number} perPage
 * @param {Number} [currentPage]
 * @returns {string}
 */
var getPagination = function (url, itemCount, perPage, currentPage = 1) {
	var pages = Math.ceil(itemCount / perPage);

	if (pages == 1) {
		return '';
	}

	var paginationTpl =
		'<ul class="pagination">' +
			'<% for (var i = 1; i <= pages; i++) { %>' +
			'<li<%= currentPage == i? \' class="active"\': "" %>>' +
				'<a href="<%= url %><%= i %>"><%= i %></a>' +
			'</li>' +
			'<% } %>' +
		'</ul>';

	return _.template(paginationTpl)({
		url: url,
		pages: pages,
		currentPage: currentPage
	})
};

export default getPagination;