Backbone = require 'backbone'
Marionette = require 'backbone.marionette'
AlbumListView = require './views/AlbumList.coffee'
Albums = require './models/Albums.coffee'
AlbumDetailView = require './views/AlbumDetail.coffee'
Album = require './models/Album.coffee'

AlbumController =
	list: () ->
		albums = new Albums();
		albums.fetch(
			reset: true
			success: () ->
				app.getRegion().show(new AlbumListView(albums))
		)
	detail: (albumId, page = 1) ->
		album = new Album(id: albumId, page: page);
		album.fetch(
			reset: true
			success: () ->
				app.getRegion().show(new AlbumDetailView(album))
		)

Router = Marionette.AppRouter.extend(
	controller: AlbumController
	appRoutes:
		'': 'list'
		'album/:albumId': 'detail'
		'album/:albumId/page': 'detail'
		'album/:albumId/page/:page': 'detail'
)

App = Marionette.Application.extend(
	region: '#app'
)

app = new App(
	routes: new Router()
)

# Start history when our application is ready
app.on('start', () ->
	Backbone.history.start()
)

# loadInitialData().then(app.start);
app.start()