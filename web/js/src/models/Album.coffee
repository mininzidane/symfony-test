Backbone = require 'backbone'

Album = Backbone.Model.extend(
	url: () -> '/ajax/album/' + @id + '/page/' + @page;

	initialize: (params) ->
		@page = params.page || 1;
)

module.exports = Album