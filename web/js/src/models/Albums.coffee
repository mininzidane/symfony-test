Backbone = require 'backbone'

Albums = Backbone.Model.extend(
	url: '/ajax/album-list'
)

module.exports = Albums