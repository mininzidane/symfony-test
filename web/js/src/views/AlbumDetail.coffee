Marionette = require 'backbone.marionette'

AlbumDetailView = Marionette.View.extend(
	initialize: (album) ->
		if album.get('status') == 'ok'
			@html = album.get('data')
		else
			@html = '<div class="alert alert-danger">' + album.get('data') + '</div>'
		@render()

	render: () ->
		@$el.html(@html)
)

module.exports = AlbumDetailView