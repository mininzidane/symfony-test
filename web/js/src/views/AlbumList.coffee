Marionette = require 'backbone.marionette'

AlbumListView = Marionette.View.extend(
	initialize: (albums) ->
		if albums.get('status') == 'ok'
			@html = albums.get('data')
		else
			@html = '<div class="alert alert-danger">' + albums.get('data') + '</div>'
		@render()

	render: () ->
		@$el.html(@html)
)

module.exports = AlbumListView